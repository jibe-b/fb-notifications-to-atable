const { chromium } = require('playwright');
const fs = require('fs');
const cheerio = require("cheerio");
const fetch = require('node-fetch');

(async () => {

  const baserow_api_key = fs.readFileSync('baserow_api_key', 'utf8').toString()
 
  const browser = await chromium.launch();
  const context = await browser.newContext()

  const cookies = fs.readFileSync('cookies.json', 'utf8')
  if (!Object.keys(cookies).length) {
    console.log("cookie available")
  }
  else {
    await context.addCookies(JSON.parse(cookies))
  }

  const page = await context.newPage();
  await page.goto("https://www.facebook.com/notifications")

  

  var scrapNotifications = async () => {
    var cached_links = fs.readFileSync("cached_links.jsonlines").toString().split("\n")
    newly_cached_links = []

    var rows = await page.$$(notifications_selector)
    console.log(rows.length)

    for (var row of rows) {
      try {
        var row_interm = await row.getProperty("innerHTML")
        var row_string = await row_interm.jsonValue()
        var $ = cheerio.load(row_string)

        var link = await $('a[role="link"]').attr('href')
      } catch {
        var link = "even not parsed"
      }

      try {
        var extractedText_origin = await $(".a8c37x1j.ni8dbmo4.stjgntxs.l9j0dhe7").html()
        var extractedText = extractedText_origin //.split("Non lu</div>").slice(1, 100).join("")
        var text = typeof extractedText !== 'undefined' ? extractedText : "NoText"

        var date = new Date()
        options = { year: 'numeric', month: 'numeric', day: 'numeric', hour: "numeric", minute: "numeric", second: "numeric"}
        var creationDate = (date).toLocaleDateString('fr-FR', options)

        var res = { link, text, creationDate }

        console.log(link, !cached_links.includes(link))
        
        if (
            !cached_links.includes(link) && link.match("^https://").length > 0
          ) {
          fetch(url, {
            method: 'POST',
            json: res,
            headers: {
              'Content-Type': 'application/json',
              'Authorization': 'Token ' + baserow_api_key
            }
          })
            .then((response) => {
              if (response.ok === false) {
                console.log("push failed")
                fs.appendFile('failed_notifs.jsonlines', res + "\n", function(err){if (err) {throw err;}})
              }
            })
            .catch((err) => {
              console.log(err)
            })
          newly_cached_links.push(link)
        }

      }
      catch {
          console.log("failed_parsing")
          //newly_cached_links.push(link)
          console.log("link cached anyway")
          try{
            fs.appendFile('failed_parsing.jsonlines', row_string.toString() + "\n", ()=>{console.log})
          }catch{
            console.log(typeof row_string)
          }
        }
      }

      for (link of newly_cached_links){
        fs.appendFile('cached_links.jsonlines', link + "\n", ()=>{console.log})
      }
      return "notifications scrapped" + creationDate.toString(options)
    }

    url = "https://api.baserow.io/api/database/rows/table/18155/"

    var notifications_selector = ".l9j0dhe7[role='row']"

    await page.waitForSelector(notifications_selector, { timeout: 10000 })

    console.log(await scrapNotifications())
    setInterval(async () => {
      console.log(await scrapNotifications())
    }, 1 + Math.random() * 2 * 60 * 1000)

    //await browser.close();
  })();


